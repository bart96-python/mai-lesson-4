import argparse
import sys

from src.utils import *
from src.logger import *


class CmdLine:
	__logger = Logger()

	def __init__(self, configDict=None):
		self.argsList = sys.argv
		self.config = configDict or utils.getConfig()

	def parse(self):
		scriptName = self.argsList[1]
		argsList = '] [--'.join(self.config.keys())
		parser = argparse.ArgumentParser(description=f'{scriptName} [--{argsList}]')

		try:
			for key, value in self.config[scriptName].items():
				helpStr = f"{value['desc']}. Пример: --{key} {value['example']}. По умолчанию {value['default']}"
				parser.add_argument(f'--{key}', default=value['default'], type=int if value['type'] == 'int' else str, help=helpStr)
		except KeyError:
			self.__logger.cmd(f'Не удалось найти конфигурацию {scriptName}', 81)

		return parser.parse_args(self.argsList[2:])
