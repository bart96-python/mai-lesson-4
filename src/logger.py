import math

from datetime import datetime


class Logger:
	__styleType = {
		'NoTitle': 'white',

		'error': 'red',
		'warn': 'yellow',
		'info': 'blue',

		'cmd': 'magenta',
		'bot': 'cyan',
		'user': 'cyan',
		'memory': 'green',
	}

	__styleList = {
		'reset': [0, 22],
		'bold': [1, 21],
		'italic': [3, 23],
		'underline': [4, 24],
		'blink': [5, 25],
		'inverse': [7, 27],
		'invisible': [8, 21],
		'strike': [9, 29],
	}

	for index, color in enumerate(['black', 'red', 'green', 'yellow', 'blue', 'magenta', 'cyan', 'white']):
		__styleList[color] = [30 + index, 39]
		__styleList[color + 'Bright'] = [90 + index, 39]

	# print({'q': 123, (
	# 	{
	# 		color: [30 + index, 39], color + 'Bright': [90 + index, 39]
	# 	} for index, color in enumerate(['black', 'red', 'green', 'yellow', 'blue', 'magenta', 'cyan', 'white'])
	# )})

	def __init__(self, **kwargs):
		self.gameName = kwargs.get('gameName') or ''
		self.fileName = kwargs.get('fileName') or 'FileName'
		self.dateTime = kwargs.get('dateTime') or '%Y-%m-%d %H:%M:%S'

	def all(self, *args):
		self.log(*args).cmd(*args)
		return self

	def log(self, text: str, styleType=3):
		title = self.__getStyle(styleType)[0]

		logFilePath = './logs/' + self.fileName + '.log'
		with open(logFilePath, 'a', encoding='utf-8') as f:
			f.write('> [{}] [{}] => ({}) {}'.format(datetime.now().strftime(self.dateTime), title, self.gameName, text))
			f.write('\n')

			return self

	def cmd(self, text: str, styleType=3):
		[title, color] = self.__getStyle(styleType)

		codes = self.__getCode(self.__styleList[color])
		codesBright = self.__getCode(self.__styleList[color + 'Bright'])
		codesWhite = self.__getCode(self.__styleList['white'])
		codesBlack = self.__getCode(self.__styleList['blackBright'])

		template = codes[0] + '> [{}] ' + codes[1]
		template += '[' + codesBright[0] + '{}' + codesBright[1] + ']'
		template += codesWhite[0] + ' \t=> ' + codesWhite[1]
		template += ' ({})' if self.gameName else '{}'
		template += codesBlack[0] + ' {}' + codesBlack[1]

		output = template.format(datetime.now().strftime(self.dateTime), title, self.gameName, text)
		print(output) if (styleType > 0) else exit(output)

		return self

	def __getStyle(self, styleType: int):
		styleTypeList = list(self.__styleType.keys())
		styleTypeModule = int(math.fabs(styleType))
		title = styleTypeList[styleTypeModule] if len(styleTypeList) > styleTypeModule else 'NoTitle'
		color = self.__styleType[title]

		return [title, color]

	@staticmethod
	def __getCode(codes):
		return [f'\x1b[{codes[0]}m', f'\x1b[{codes[1]}m']
