import json
import inspect


class Utils:
	def __init__(self):
		self.__config = self.__getConfig()

	def getConfig(self):
		return self.__config[inspect.stack()[1].filename.split('\\')[-1].split('.')[0]]

	@staticmethod
	def __getConfig():
		with open('./config.json', 'r', encoding='utf-8') as data:
			return json.load(data) or {}


utils = Utils()
